import React, { useState, useEffect } from 'react'
import avatar from '../../assets/player.png'
import './opponent.css'


const Opponent = ({socket, turn, limit}) => {
    const [opponents, setOpponents] = useState([])

    useEffect(() => {
        socket.on('send_opponent', (opponents) => {
            setOpponents(opponents)
        })
    }, [socket])

    return (
    <div>
        {(limit === 2) 
            ?
            //Only 2 players
            opponents.map((opponent) => {

                //For inactive players who have disconnected or failed to declare.
                if (!opponent.tableStatus) {
                    return (
                        <div className={`OpponentContainer normalPosition inactiveOpponent`}>
                            <img style={{width: '30px', height: '30px', color:'white'}} className='avatar' src={avatar}></img>
                            <div className='playerInfo'>
                                <p>{opponent.username}</p>
                            </div>
                            <div>
                                {/* Empty Div */}
                            </div>
                        </div>
                    )
                }

                return (
                <div className={(turn === opponent.socket) ? `OpponentContainer normalPosition Turn` : `OpponentContainer normalPosition`}>
                    <img style={{width: '30px', height: '30px', color:'white'}} className='avatar' src={avatar}></img>
                    <div className='playerInfo'>
                        <p>{opponent.username}</p>
                    </div>
                    <div>
                        {/* Empty Div */}
                    </div>
                </div>
                )
            })
            :
            //More than 2 players
            opponents.map((opponent, i) => {
                let seat
                switch(i) {
                    case 0: 
                        seat = 'seatOne';
                        break;
                    case 1:
                        seat = 'seatTwo';
                        break;
                    case 2:
                        seat = 'seatThree';
                        break;
                    case 3:
                        seat = 'seatFour';
                        break;
                    case 4:
                        seat = 'seatFive';
                        break;
                }
                //For inactive players who have disconnected or failed to declare.
                if (!opponent.tableStatus) {
                    return (
                        <div className={`OpponentContainer ${seat} inactiveOpponent`}>
                            <img style={{width: '30px', height: '30px', color:'white'}} className='avatar' src={avatar}></img>
                            <div className='playerInfo'>
                                <p>{opponent.username}</p>
                            </div>
                            <div>
                                {/* Empty Div */}
                            </div>
                        </div>
                    )
                }

                return (
                <div className={(turn === opponent.socket) ? `OpponentContainer ${seat} Turn` : `OpponentContainer ${seat}`}>
                    <img style={{width: '30px', height: '30px', color:'white'}} className='avatar' src={avatar}></img>
                    <div className='playerInfo'>
                        <p>{opponent.username}</p>
                    </div>
                    <div>
                        {/* Empty Div */}
                    </div>
                </div>
                )
            })
        }
    </div>
    )
}

export default Opponent