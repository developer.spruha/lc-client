import React, { useEffect, useState } from 'react'
import './Table.css'
import backside from '../../assets/back.jpeg'

const Table = ({socket, turn, history, turnPlayed}) => {
    const [joker, setJoker] = useState([])
    const [dropped, setDropped] = useState([])

    const drawCardFunction = () => {
        //The order in which these sockets are called is super important.
        socket.emit('draw_card')
        socket.emit('get_score')
        socket.emit('add_to_history')
        socket.emit('shift_dropped_pile')
    }

    const drawFromSecond = () => {
        //The order in which these sockets are called is super important.
        socket.emit('draw_card_from_second')
        socket.emit('get_score')
        socket.emit('add_to_history')
        socket.emit('shift_dropped_pile')
    }

    useEffect(() => {
        socket.on('send_joker', (joker) => {
            setJoker(joker)
        })

        socket.on('card_dropped', (card) => {
            // console.log('Card dropped is: ', card)
            setDropped([card])
        })

        socket.on('clear_table', (data) => {
            setDropped(null)
        })

        // console.log(joker)
        // console.log(dropped)
    }, [socket])

    return (
        <div className='tableContainer'>
            <div className='tableCards'>
                <div className='deck'>
                    {((turn === socket.id) && turnPlayed) ?
                    //If it is users turn and he has already played, he can draw a card
                    <img onClick={drawCardFunction} className='deckCard' src={backside} alt="error"></img> 
                    : 
                    //Otherwise he cannot draw a card
                    <img className='deckCard' src={backside} alt="error"></img>}
                    {joker && joker.map((joker, i) => {
                        let src = require('../../assets/cards/' + joker +'.png')
                        return <img className='jokerCard' key={i} src={src} alt="error"></img>
                    })}
                    {/* <img className='jokerCard' src={backside}></img> */}
                </div>
                <div className='dropped'>
                    {
                    dropped ? 
                    dropped.map((card, i) => {
                        let src = require('../../assets/cards/' + card +'.png')
                        return <img className='droppedCard' key={i} src={src} alt="error"></img>
                    })
                    : 
                    null
                    }
                </div>
                <div className='passed'>
                    {   turnPlayed ?
                        //If turn Played, user can draw from second pile
                        (history.length >= 2) ? history.slice(-1).map((item, i) => {
                            let src = require('../../assets/cards/' + item +'.png')
                            return <img onClick={drawFromSecond} className='passedCard' key={i} src={src} alt="error"></img>
                        }) :
                        history.slice(-1).map((item, i) => {
                            let src = require('../../assets/cards/' + item +'.png')
                            return <img onClick={drawFromSecond} className='passedCard' key={i} src={src} alt="error"></img>
                        })
                        :
                        //Else he cannot draw.
                        (history.length >= 2) ? history.slice(-1).map((item, i) => {
                            let src = require('../../assets/cards/' + item +'.png')
                            return <img className='passedCard' key={i} src={src} alt="error"></img>
                        }) :
                        history.slice(-1).map((item, i) => {
                            let src = require('../../assets/cards/' + item +'.png')
                            return <img className='passedCard' key={i} src={src} alt="error"></img>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Table