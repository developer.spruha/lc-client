import React, { useEffect, useState } from 'react'
import './cards.css'

const Cards = ({socket, turn, updateLastCard, updateTurnPlayed}) => {
    const [cards, setCards] = useState([])
    const [myPreviousCard, setMyPreviousCard] = useState(null)

    const dropCard = (e, card, i) => {
        // setCardsPlayed([...cardsPlayed, card])
        socket.emit('card_played', card)
        setCards(cards.filter((item, index) => index !== i))
        // setCards(cards.splice(i, 1))
        socket.emit('get_score')
        setMyPreviousCard([card])
        updateLastCard(card)
        updateTurnPlayed(false)
    }

    useEffect(() => {
        socket.on('send_cards', (player) => {
            setCards(player.hand)
            // console.log('Cards are: ', player.hand)
        })

        socket.on('take_card', (newHand) => {
            setCards(newHand)
            setMyPreviousCard(null)
        })

        socket.on('turn_is', () => {
            setMyPreviousCard(null)
        })

    }, [socket])

    return (
        <div className='cardsContainer'>
            {
                ((turn === socket.id)) ? 
                cards.map((card, i) => {
                    let src = require('../../assets/cards/' + card +'.png')
                    if (myPreviousCard) {
                        if ((card[1] === myPreviousCard[0][1])) {
                            return <img onClick={(e) => dropCard(e, card, i)} className='card' key={i} src={src}></img>
                        } else {
                            return <img className='card' key={i} src={src}></img>
                        }
                    } else {
                        return <img onClick={(e) => dropCard(e, card, i)} className='card' key={i} src={src}></img>
                    }
                })
                : 
                cards.map((card, i) => {
                    let src = require('../../assets/cards/' + card +'.png')
                    return <img className='card' key={i} src={src}></img>
                })
            }
        </div>
    )
}

export default Cards