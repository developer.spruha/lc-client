import React, { useState, useEffect } from 'react'
import avatar from '../../assets/player.png'
import './player.css'

const Player = ({turn, username, socket, endTurnBtn, turnPlayed}) => {
    const [score, setScore] = useState(0)

    const endTurn = () => {
        socket.emit('end_turn')
        socket.emit('shift_dropped_pile')
    }

    const showCards = () => {
        socket.emit('show_cards')
    }

    useEffect(() => {
        socket.on('send_score', (score) => {
            setScore(score)
        })
    }, [socket])

    return (
        <>
        <div className='endTurnContainer'>
            {   
                (endTurnBtn && (turn === socket.id))
                ? 
                <button onClick={endTurn} className='endTurn'>END TURN</button>
                :
                null
            }
        </div>
        <div className={(turn === socket.id) ? 'PlayerContainer Turn' : 'PlayerContainer'}>
            <div style={{background: 'white', borderRadius:'50px'}}>
                <img style={{width: '30px', height: '30px', color:'white'}} className='avatar' src={avatar}></img>
            </div>
            <p>{username}</p>
            <p className='scoreText'>{score}</p>
        </div>
        <div className='showContainer'>
            {
                (score <= 10 && !turnPlayed && (turn === socket.id)) 
                ?
                <button onClick={showCards} className='showBtn'>SHOW</button>
                :
                null
            }
            
        </div>
        </>
    )
}

export default Player