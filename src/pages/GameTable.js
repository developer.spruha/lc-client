import React, { useState, useEffect } from 'react'
import styles from './css/table.module.css'
import Player from '../components/Player/Player'
import Cards from '../components/Cards/Cards'
import Opponent from '../components/Opponent/Opponent'
import Table from '../components/Table/Table'
import Declaration from './Declaration'

const GameTable = ({socket, username, limit, tableID}) => {
    const [turn, setTurn] = useState(false)
    const [turnPlayed, setTurnPlayed] = useState(false)
    const [history, setHistory] = useState([])
    const [endTurnBtn, setEndTurnBtn] = useState(null)
    const [lastCard, setLastCard] = useState(null)
    const [declared, setDeclared] = useState(null)

    const updateLastCard = (card) => {
        if (card[1] === lastCard[1]) {
            setEndTurnBtn(true)
        }
        console.log('From GameTable.js, card[1] === lastCard[1] is ', card[1] === lastCard[1])
        console.log('lastCard = ', lastCard)
        console.log('card =', card)
    }

    const updateTurnPlayed = () => {
        setTurnPlayed(true)
    }

    useEffect(() => {
        setDeclared(null)
        socket.emit('get_cards')
        socket.emit('get_opponent')
        // socket.emit('get_joker')
        socket.on('turn_is', (player) => {
            setEndTurnBtn(false)
            setTurnPlayed(false)
            setTurn(player)
            // console.log('Turn is mine: ', player === socket.id)
            // console.log("My socket id is: ", socket.id)
        })
        socket.emit('get_score')

        socket.on('send_history', (data) => {
            setHistory(data)
            setLastCard(data.slice(-1)[0])
            // console.log('History given is: ', data)
        })

        socket.on('clear_table', (freshPile) => {
            setHistory(freshPile)
            setLastCard(freshPile[0])
        })

        socket.on('ask_for_opponents_again', () => {
            socket.emit('get_opponent')
        })

        socket.on('direct_victory', () => {
            setDeclared('W')
        })

        if (limit === 2) {
            socket.on('winner_is', (winner) => {
                if (winner === socket.id) {
                    setDeclared('W')
                } else {
                    setDeclared('L')
                }
            })
        } else {
            socket.on('send_declaration', (result) => {
                if (result.declare) {
                    if (result.socket === socket.id) {
                        setDeclared('W')
                    } else {
                        setDeclared('L')
                    }
                } else {
                    if (result.socket === socket.id) {
                        setDeclared('L')
                        socket.emit('exit_game_table', tableID)
                    }
                }
            })
        }
        

        // console.log(turn)
        // console.log(history)
    }, [socket])

    return (
        <>
        {!declared ? 
        <div className={styles.tableContainer}>
            <Opponent socket={socket} turn={turn} limit={limit}/>
            <Table socket={socket} turn={turn} history={history} turnPlayed={turnPlayed}/>
            <Player username={username} socket={socket} turn={turn} endTurnBtn={endTurnBtn} turnPlayed={turnPlayed}/>
            <Cards socket={socket} turn={turn} history={history} updateLastCard={updateLastCard} updateTurnPlayed={updateTurnPlayed}/>
        </div>
        :
        <Declaration declared={declared}/>
        }
        </>
    )
}

export default GameTable