import React, { useState } from 'react'
import Waiting from './Waiting'
import io from 'socket.io-client'
import './css/Lobby.css'

const socket = io.connect("http://localhost:3001");

const Lobby = () => {
    const [username, setUsername] = useState('')
    const [tableID, setTableID] = useState('')
    const [limit, setLimit] = useState(2)
    const [twoActive, setTwoActive] = useState('activeLimit')
    const [sixActive, setSixActive] = useState('')
    const [waiting, setWaiting] = useState(false)

    const joinTable = () => {
        // console.log('Table Joined')
        socket.emit('join_table', username, tableID)
        setWaiting(true)
    }

    return (
        <div>
            
            {!waiting ?
            <>
            <form className='joinForm' onSubmit={joinTable}>
                <h1 className='title'>LEAST COUNT</h1>
                <input type="text" value={username} placeholder="Username" onChange={(e) => setUsername(e.target.value)} required></input>
                <input type="text" value={tableID} placeholder="Game Table" onChange={(e) => setTableID(e.target.value)} required></input>
                <div className='playerLimit'>
                    <div className={'limitBtn ' + twoActive} onClick={() => 
                        {setLimit(2); setTwoActive('activeLimit'); setSixActive('')}}
                    >
                        2 Players
                    </div>
                    <div className={'limitBtn ' + sixActive} onClick={() => 
                        {setLimit(6); setSixActive('activeLimit'); setTwoActive('')}}
                    >
                        6 Players
                    </div>
                </div>
                <button className='joinTableBtn' type="submit">Join Table</button>
            </form> 
            </>
            :
            <Waiting username={username} tableID={tableID} limit={limit} socket={socket}/>
            }
            
        </div>
    )
}

export default Lobby