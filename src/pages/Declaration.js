import React from 'react'
import './css/declaration.css'

const Declaration = ({declared}) => {
    return (
        <div>
            {   (declared === 'W') 
                ?
                <div className='wonDiv'>
                    <p className='wonText'>You Won!</p>
                </div>
                :
                <div className='lostDiv'>
                    <p className='lostText'>You Lost!</p>
                </div>
            }
        </div>
    )
}

export default Declaration