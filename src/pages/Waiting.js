import React, { useEffect, useState } from 'react'
import GameTable from '../pages/GameTable'
import styles from "./css/table.module.css"

const Waiting = ({socket, tableID, username, limit}) => {
    const [gameStart, setGameStart] = useState(false)

    useEffect(() => {
        console.log('joined room')

        socket.emit('room_joined', tableID, limit)

        socket.on('start_game', () => {
            setGameStart(true)
        })
    }, [socket])

    return (
        <div>
        {!gameStart ? 
        <div className={styles.tableContainer}>
            <h3 className={styles.waitingText}>Waiting for other players to join...</h3>
        </div>
        :
        <GameTable username={username} tableID={tableID} socket={socket} limit={limit}/>
        }
        </div>
    )
}

export default Waiting